package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.model.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();


    private static final Date TODAY = new Date();

    private List<Session> sessions;

    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessions = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setDate(TODAY);
            if (i <= 5)
                session.setUserId(USER_ALFA_ID);
            else
                session.setUserId(USER_BETA_ID);
            sessions.add(session);
            sessionRepository.add(session);
        }
    }

    @After
    public void clearRepository() {
        sessionRepository.removeAll();
        sessions.clear();
    }

    @Test
    public void addAllTest() {
        ArrayList<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session newSession = new Session();
            newSession.setDate(new Date());
            newSessions.add(newSession);
        }
        sessionRepository.addAll(newSessions);
        sessions.addAll(newSessions);
        assertEquals(sessions, sessionRepository.findAll());
    }

    @Test
    public void setTest() {
        ArrayList<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session newSession = new Session();
            newSession.setDate(new Date());
            newSessions.add(newSession);
        }
        sessionRepository.set(newSessions);
        sessions.clear();
        sessions.addAll(newSessions);
        assertEquals(sessions, sessionRepository.findAll());
    }

    @Test
    public void findOneByIndexTest() {
        assertNull(sessionRepository.removeOneByIndex(-1));
        assertNull(sessionRepository.removeOneByIndex(sessions.size()));
        assertEquals(sessions.get(0), sessionRepository.removeOneByIndex(0));
        sessions.remove(0);
        assertEquals(sessions, sessionRepository.findAll());
    }

    @Test
    public void removeOneByIdTest() {
        assertNull(sessionRepository.removeOneById(NONE_ID));
        for (Session session : sessions) {
            assertEquals(session, sessionRepository.removeOneById(session.getId()));
        }
        sessions.clear();
        assertEquals(sessions, sessionRepository.findAll());
    }

}
