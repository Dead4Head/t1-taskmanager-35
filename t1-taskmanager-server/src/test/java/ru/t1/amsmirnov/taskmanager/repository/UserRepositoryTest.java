package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private List<User> users;

    @Before
    public void initRepository() {
        userRepository = new UserRepository();
        users = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("First Name " + i);
            user.setLastName("Last Name " + i);
            user.setMiddleName("Middle Name " + i);
            user.setEmail("user" + i + "@tm.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            users.add(user);
        }
    }

    @After
    public void clearRepository() {
        userRepository.removeAll();
        users.clear();
    }

    @Test
    public void testFindByEmail() {
        for (final User user : users) {
            assertEquals(user, userRepository.findOneByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByLogin() {
        for (final User user : users) {
            assertEquals(user, userRepository.findOneByLogin(user.getLogin()));
        }
    }

    @Test
    public void testIsEmailExist() {
        for (final User user : users) {
            assertTrue(userRepository.isEmailExist(user.getEmail()));
            assertFalse(userRepository.isEmailExist(""));
        }
    }

    @Test
    public void testIsLoginExist() {
        for (final User user : users) {
            assertTrue(userRepository.isLoginExist(user.getLogin()));
            assertFalse(userRepository.isEmailExist(user.getLogin() + user.getLogin()));
        }
    }

}
