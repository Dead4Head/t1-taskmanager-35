package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IUserOwnedService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<M> records;
        if (comparator == null) records = repository.findAll(userId);
        else records = repository.findAll(userId, comparator);
        return records;
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @Nullable final M removedModel = repository.removeOne(userId, model);
        if (removedModel == null) throw new ModelNotFoundException();
        return removedModel;
    }

    @NotNull
    @Override
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.removeOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model = repository.removeOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public boolean existById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(userId, id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }


}
