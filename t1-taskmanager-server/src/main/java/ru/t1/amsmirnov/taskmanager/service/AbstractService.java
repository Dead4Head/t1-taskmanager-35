package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> addAll(@Nullable Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        return repository.addAll(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        @Nullable final List<M> records = repository.findAll();
        if (records == null) throw new ModelNotFoundException();
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @NotNull final List<M> records = repository.findAll(comparator);
        return records;
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) {
            removeAll();
            return;
        }
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @Nullable M deletedModel = repository.removeOne(model);
        if (deletedModel == null) throw new ModelNotFoundException();
        return deletedModel;
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M deletedModel = repository.removeOneById(id);
        if (deletedModel == null) throw new ModelNotFoundException();
        return deletedModel;
    }

    @NotNull
    @Override
    public M removeOneByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable M deletedModel = repository.removeOneByIndex(index);
        if (deletedModel == null) throw new ModelNotFoundException();
        return deletedModel;
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(id);
    }

}
