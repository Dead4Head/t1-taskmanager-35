package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existById(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    int getSize(@NotNull String userId);

}
