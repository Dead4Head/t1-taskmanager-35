package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ISessionService;
import ru.t1.amsmirnov.taskmanager.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
