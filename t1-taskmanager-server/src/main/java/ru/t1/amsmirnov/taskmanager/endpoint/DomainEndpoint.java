package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.response.data.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64LoadResponse(e);
        }
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64SaveResponse(e);
        }
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinLoadResponse loadDataBin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinLoadRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBin();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinLoadResponse(e);
        }
        return new DataBinLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinSaveResponse saveDataBin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinSaveRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBin();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinSaveResponse(e);
        }
        return new DataBinSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadFasterXMLResponse(e);
        }
        return new DataJsonLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveFasterXMLResponse(e);
        }
        return new DataJsonSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxbResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxbRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadJaxbResponse(e);
        }
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxbResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxbRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveJaxbResponse(e);
        }
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXMLResponse loadDataXmlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadFasterXMLResponse(e);
        }
        return new DataXmlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXMLResponse saveDataXmlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveFasterXMLResponse(e);
        }
        return new DataXmlSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXMLJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadJaxBResponse(e);
        }
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXMLJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveJaxBResponse(e);
        }
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXMLResponse loadDataYAMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataYAMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlLoadFasterXMLResponse(e);
        }
        return new DataYamlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXMLResponse saveDataYAMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXMLRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataYAMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlSaveFasterXMLResponse(e);
        }
        return new DataYamlSaveFasterXMLResponse();

    }

}
