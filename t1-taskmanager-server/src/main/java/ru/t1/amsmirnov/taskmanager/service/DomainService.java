package ru.t1.amsmirnov.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.IDomainService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.DumpFileException;
import ru.t1.amsmirnov.taskmanager.exception.system.TransportFileException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BIN = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public void loadDataBackup() throws AbstractException {
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
            @Nullable final String base64Date = new String(base64Byte);
            @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
            @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            setDomain(domain);
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BACKUP, exception);
        }
    }

    @Override
    public void saveDataBackup() throws AbstractException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();
        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            byteArrayOutputStream.close();

            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(base64.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BACKUP, exception);
        }
    }

    @Override
    public void loadDataBase64() throws AbstractException {
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Date = new String(base64Byte);
            @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
            @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            setDomain(domain);
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BASE64, exception);
        }
    }

    @Override
    public void saveDataBase64() throws AbstractException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();
        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            byteArrayOutputStream.close();

            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(base64.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BASE64, exception);
        }
    }

    @Override
    public void loadDataBin() throws AbstractException {
        try {
            @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BIN);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            setDomain(domain);
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BIN, exception);
        }
    }

    @Override
    public void saveDataBin() throws AbstractException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BIN);
        @NotNull final Path path = file.toPath();

        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BIN, exception);
        }
    }

    @Override
    public void loadDataJsonFasterXML() throws AbstractException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
            @NotNull final String json = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

    @Override
    public void saveDataJsonFasterXML() throws AbstractException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_JSON);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

    @Override
    public void loadDataJsonJaxB() throws AbstractException {
        try {
            System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

    @Override
    public void saveDataJsonJaxB() throws AbstractException {
        try {
            System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_JSON);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

    @Override
    public void loadDataXMLFasterXML() throws AbstractException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
            @NotNull final String xml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_XML, exception);
        }
    }

    @Override
    public void saveDataXMLFasterXML() throws AbstractException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_XML, exception);
        }
    }

    @Override
    public void loadDataXMLJaxB() throws AbstractException {
        try {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_XML, exception);
        }
    }

    @Override
    public void saveDataXMLJaxB() throws AbstractException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_XML, exception);
        }
    }

    @Override
    public void loadDataYAMLFasterXML() throws AbstractException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
            @NotNull final String yaml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_YAML, exception);
        }
    }

    @Override
    public void saveDataYAMLFasterXML() throws AbstractException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_YAML);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(yaml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (final Exception exception) {
            throw new TransportFileException(FILE_YAML, exception);
        }
    }


    public Domain getDomain() throws AbstractException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws AbstractException {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
    }

}
