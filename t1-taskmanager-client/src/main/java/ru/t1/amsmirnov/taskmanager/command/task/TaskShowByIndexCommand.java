package ru.t1.amsmirnov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskShowByIndexRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskShowByIndexResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken(), index);
        @NotNull final TaskShowByIndexResponse response = getTaskEndpoint().findOneTaskByIndex(request);
        if (!response.isSuccess() || response.getTask() == null)
            throw new CommandException(response.getMessage());
        showTask(response.getTask());
    }

}
