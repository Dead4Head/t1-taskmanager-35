package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse() {
    }

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

    public UserRemoveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}