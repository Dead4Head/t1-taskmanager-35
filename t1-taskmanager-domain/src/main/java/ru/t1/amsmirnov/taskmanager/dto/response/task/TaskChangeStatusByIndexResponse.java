package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse() {
    }

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskChangeStatusByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}