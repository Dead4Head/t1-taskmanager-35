package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull String NAME = "ProjectEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectClearResponse removeAllProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectClearRequest request);

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCompleteByIdRequest request);

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCreateRequest request);

    @NotNull
    @WebMethod
    ProjectListResponse findAllProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectListRequest request);

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeOneProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIdRequest request);

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeOneProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectShowByIdResponse findOneProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowByIdRequest request);

    @NotNull
    @WebMethod
    ProjectShowByIndexResponse findOneProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectStartByIdRequest request);

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectStartByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIdRequest request);

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIndexRequest request);

}
