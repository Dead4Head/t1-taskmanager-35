package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest() {
    }

    public UserRegistryRequest(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    public void setLogin(@Nullable final String login) {
        this.login = login;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable final String password) {
        this.password = password;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

}