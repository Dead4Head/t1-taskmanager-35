package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse() {
    }

    public UserProfileResponse(@Nullable final User user) {
        super(user);
    }

    public UserProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}