package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractModel implements Serializable {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

}
